<?php
/**
 * @file
 * entity_rdf_demo.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function entity_rdf_demo_taxonomy_default_vocabularies() {
  return array(
    'cuisines' => array(
      'name' => 'Cuisines',
      'machine_name' => 'cuisines',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
    'tags' => array(
      'name' => 'Tags',
      'machine_name' => 'tags',
      'description' => 'Use tags to group articles on similar topics into categories.',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
  );
}
