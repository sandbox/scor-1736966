<?php
/**
 * @file
 * entity_rdf_demo.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function entity_rdf_demo_user_default_permissions() {
  $permissions = array();

  // Exported permission: access resource comment.
  $permissions['access resource comment'] = array(
    'name' => 'access resource comment',
    'roles' => array(
      0 => 'administrator',
      1 => 'anonymous user',
    ),
    'module' => 'restws',
  );

  // Exported permission: access resource file.
  $permissions['access resource file'] = array(
    'name' => 'access resource file',
    'roles' => array(
      0 => 'administrator',
      1 => 'anonymous user',
    ),
    'module' => 'restws',
  );

  // Exported permission: access resource node.
  $permissions['access resource node'] = array(
    'name' => 'access resource node',
    'roles' => array(
      0 => 'administrator',
      1 => 'anonymous user',
    ),
    'module' => 'restws',
  );

  // Exported permission: access resource taxonomy_term.
  $permissions['access resource taxonomy_term'] = array(
    'name' => 'access resource taxonomy_term',
    'roles' => array(
      0 => 'administrator',
      1 => 'anonymous user',
    ),
    'module' => 'restws',
  );

  // Exported permission: access resource taxonomy_vocabulary.
  $permissions['access resource taxonomy_vocabulary'] = array(
    'name' => 'access resource taxonomy_vocabulary',
    'roles' => array(
      0 => 'administrator',
      1 => 'anonymous user',
    ),
    'module' => 'restws',
  );

  // Exported permission: access resource user.
  $permissions['access resource user'] = array(
    'name' => 'access resource user',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'restws',
  );

  return $permissions;
}
