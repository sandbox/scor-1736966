<?php
/**
 * @file
 * entity_rdf_demo.features.uuid_node.inc
 */

/**
 * Implements hook_uuid_features_default_content().
 */
function entity_rdf_demo_uuid_features_default_content() {
  $nodes = array();

  $nodes[] = array(
  'uid' => '1',
  'title' => 'Schlösselgarten',
  'log' => '',
  'status' => '1',
  'comment' => '2',
  'promote' => '1',
  'sticky' => '0',
  'vuuid' => '996e79e4-fa93-82d4-35ef-6dfea37a6023',
  'type' => 'restaurant',
  'language' => 'en',
  'created' => '1345226078',
  'tnid' => '0',
  'translate' => '0',
  'uuid' => '3e98f157-9684-e564-5138-2db368d6e03e',
  'revision_uid' => '1',
  'field_cuisine' => array(
    'und' => array(
      0 => array(
        'tid' => '4',
      ),
    ),
  ),
  'field_reservations' => array(
    'und' => array(
      0 => array(
        'value' => 'Yes',
      ),
    ),
  ),
  'field_menu_link' => array(
    'und' => array(
      0 => array(
        'url' => 'http://www.schloesselgarten.com/menue.php',
        'title' => NULL,
        'attributes' => array(),
      ),
    ),
  ),
  'field_telephone' => array(
    'und' => array(
      0 => array(
        'value' => '089 - 915452',
        'format' => NULL,
        'safe_value' => '089 - 915452',
      ),
    ),
  ),
  'field_address' => array(
    'und' => array(
      0 => array(
        'country' => 'DE',
        'administrative_area' => NULL,
        'sub_administrative_area' => NULL,
        'locality' => 'München',
        'dependent_locality' => NULL,
        'postal_code' => '81925',
        'thoroughfare' => 'Cosimastraße 41',
        'premise' => '',
        'sub_premise' => NULL,
        'organisation_name' => NULL,
        'name_line' => NULL,
        'first_name' => NULL,
        'last_name' => NULL,
        'data' => NULL,
      ),
    ),
  ),
  'cid' => '0',
  'last_comment_name' => NULL,
  'last_comment_uid' => '1',
  'comment_count' => '0',
  'name' => 'admin',
  'picture' => '0',
  'data' => 'b:0;',
  'date' => '2012-08-17 13:54:38 -0400',
);
  $nodes[] = array(
  'uid' => '1',
  'title' => 'Sound Bites',
  'log' => '',
  'status' => '1',
  'comment' => '2',
  'promote' => '1',
  'sticky' => '0',
  'vuuid' => '0f6e0e11-d218-e3f4-5dee-24ed3a70401b',
  'type' => 'restaurant',
  'language' => 'en',
  'created' => '1345309365',
  'tnid' => '0',
  'translate' => '0',
  'uuid' => '6f000cbf-e896-b044-41cf-70496050fd80',
  'revision_uid' => '1',
  'field_cuisine' => array(
    'und' => array(
      0 => array(
        'tid' => '5',
      ),
    ),
  ),
  'field_reservations' => array(
    'und' => array(
      0 => array(
        'value' => 'Yes',
      ),
    ),
  ),
  'field_menu_link' => array(
    'und' => array(
      0 => array(
        'url' => 'http://www.soundbitesrestaurant.com/food_menu.php?selected=menu',
        'title' => NULL,
        'attributes' => array(),
      ),
    ),
  ),
  'field_telephone' => array(
    'und' => array(
      0 => array(
        'value' => '(617) 623-8338',
        'format' => NULL,
        'safe_value' => '(617) 623-8338',
      ),
    ),
  ),
  'field_address' => array(
    'und' => array(
      0 => array(
        'country' => 'US',
        'administrative_area' => 'MA',
        'sub_administrative_area' => NULL,
        'locality' => 'Somerville',
        'dependent_locality' => NULL,
        'postal_code' => '02144',
        'thoroughfare' => '704 Broadway Ball Square',
        'premise' => '',
        'sub_premise' => NULL,
        'organisation_name' => NULL,
        'name_line' => NULL,
        'first_name' => NULL,
        'last_name' => NULL,
        'data' => NULL,
      ),
    ),
  ),
  'cid' => '0',
  'last_comment_name' => NULL,
  'last_comment_uid' => '1',
  'comment_count' => '0',
  'name' => 'admin',
  'picture' => '0',
  'data' => 'b:0;',
  'date' => '2012-08-18 13:02:45 -0400',
);
  $nodes[] = array(
  'uid' => '1',
  'title' => 'DrupalCon Munich',
  'log' => '',
  'status' => '1',
  'comment' => '2',
  'promote' => '1',
  'sticky' => '0',
  'vuuid' => '52c35cf4-b46a-97a4-41f8-bf263af67a5f',
  'type' => 'event',
  'language' => 'en',
  'created' => '1345226773',
  'tnid' => '0',
  'translate' => '0',
  'uuid' => 'c8010d7e-7fae-3464-e555-86ba77529fbd',
  'revision_uid' => '1',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => 'DrupalCon is an international event that brings together the people who use, develop, design, and support the Drupal platform. More than just another trade show or industry conference, it’s a shared experience that seeks to inspire and engage. DrupalCon Munich will feature dozens of curated sessions and panels from some of the most influential people and brightest minds within the Drupal community and beyond, as well as countless opportunities for networking, code sprints, informal conversations, and more. Whether you’re new to the community, have been around a while, or are just curious to see what all the fuss is about - we have a place for you.',
        'summary' => '',
        'format' => 'filtered_html',
        'safe_value' => '<p>DrupalCon is an international event that brings together the people who use, develop, design, and support the Drupal platform. More than just another trade show or industry conference, it’s a shared experience that seeks to inspire and engage. DrupalCon Munich will feature dozens of curated sessions and panels from some of the most influential people and brightest minds within the Drupal community and beyond, as well as countless opportunities for networking, code sprints, informal conversations, and more. Whether you’re new to the community, have been around a while, or are just curious to see what all the fuss is about - we have a place for you.</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_address' => array(
    'und' => array(
      0 => array(
        'country' => 'DE',
        'administrative_area' => NULL,
        'sub_administrative_area' => NULL,
        'locality' => 'München',
        'dependent_locality' => NULL,
        'postal_code' => '81925',
        'thoroughfare' => 'Arabellastraße 6',
        'premise' => '',
        'sub_premise' => NULL,
        'organisation_name' => NULL,
        'name_line' => NULL,
        'first_name' => NULL,
        'last_name' => NULL,
        'data' => NULL,
      ),
    ),
  ),
  'field_link' => array(
    'und' => array(
      0 => array(
        'url' => 'http://munich2012.drupal.org/',
        'title' => '',
        'attributes' => array(),
      ),
    ),
  ),
  'field_image' => array(
    'und' => array(
      0 => array(
        'fid' => '4',
        'alt' => 'alt',
        'title' => 'title',
        'width' => '262',
        'height' => '264',
        'uid' => '1',
        'filename' => 'drupalcon-munich-logo.png',
        'uri' => 'public://drupalcon-munich-logo.png',
        'filemime' => 'image/png',
        'filesize' => '116341',
        'status' => '1',
        'timestamp' => '1345265703',
        'uuid' => 'fed91cc0-e38f-0864-c555-ef8e20c6e5ac',
      ),
    ),
  ),
  'field_date' => array(
    'und' => array(
      0 => array(
        'value' => '2012-08-20 00:00:00',
        'value2' => '2012-08-24 00:00:00',
        'timezone' => 'America/New_York',
        'timezone_db' => 'America/New_York',
        'date_type' => 'datetime',
      ),
    ),
  ),
  'cid' => '0',
  'last_comment_name' => NULL,
  'last_comment_uid' => '1',
  'comment_count' => '0',
  'name' => 'admin',
  'picture' => '0',
  'data' => 'b:0;',
  'date' => '2012-08-17 14:06:13 -0400',
);
  $nodes[] = array(
  'uid' => '1',
  'title' => 'Nulla faucibus convallis nunc',
  'log' => '',
  'status' => '1',
  'comment' => '2',
  'promote' => '1',
  'sticky' => '0',
  'vuuid' => '4a925718-e85f-4744-8da7-9dece47218a2',
  'type' => 'article',
  'language' => 'en',
  'created' => '1345267128',
  'tnid' => '0',
  'translate' => '0',
  'uuid' => 'dbe71b6d-fc33-ce44-4583-28a4dd36e6d0',
  'revision_uid' => '0',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => 'Nunc in est sem. Ut id enim auctor enim eleifend semper ac ut turpis. Vivamus mollis risus ac magna commodo in convallis arcu imperdiet? Mauris et ultricies magna. Nullam in ligula sed massa volutpat viverra et eu orci! Nunc quis ligula leo. Cras vehicula nisi non nisi bibendum fringilla.

Duis iaculis augue dictum magna euismod vulputate. Phasellus orci libero, rutrum accumsan accumsan id, consectetur eget metus? Etiam erat leo, dictum ac gravida sit amet, congue vehicula sem. Maecenas interdum, purus eget iaculis consequat, risus mauris elementum justo, sit amet ullamcorper lectus risus sit amet diam. Vivamus sed ipsum risus, eget pulvinar mi. Nam adipiscing; neque nec ullamcorper tristique, ipsum felis viverra est, sit amet lacinia magna odio id ante. In sollicitudin mauris ut ipsum lobortis egestas. Nullam sit amet enim et arcu rhoncus fermentum et et leo. Duis et orci purus, mattis cursus erat. Sed non metus risus, eu cursus nisi? Integer accumsan facilisis orci, eu dapibus nibh lobortis a. In nec nisi sed lectus mattis ullamcorper. Curabitur erat lacus, pretium quis blandit a, convallis non nisl! Quisque non eros orci, id scelerisque lacus! Nam rutrum scelerisque viverra fusce.
',
        'summary' => '',
        'format' => 'filtered_html',
        'safe_value' => '<p>Nunc in est sem. Ut id enim auctor enim eleifend semper ac ut turpis. Vivamus mollis risus ac magna commodo in convallis arcu imperdiet? Mauris et ultricies magna. Nullam in ligula sed massa volutpat viverra et eu orci! Nunc quis ligula leo. Cras vehicula nisi non nisi bibendum fringilla.</p>
<p>Duis iaculis augue dictum magna euismod vulputate. Phasellus orci libero, rutrum accumsan accumsan id, consectetur eget metus? Etiam erat leo, dictum ac gravida sit amet, congue vehicula sem. Maecenas interdum, purus eget iaculis consequat, risus mauris elementum justo, sit amet ullamcorper lectus risus sit amet diam. Vivamus sed ipsum risus, eget pulvinar mi. Nam adipiscing; neque nec ullamcorper tristique, ipsum felis viverra est, sit amet lacinia magna odio id ante. In sollicitudin mauris ut ipsum lobortis egestas. Nullam sit amet enim et arcu rhoncus fermentum et et leo. Duis et orci purus, mattis cursus erat. Sed non metus risus, eu cursus nisi? Integer accumsan facilisis orci, eu dapibus nibh lobortis a. In nec nisi sed lectus mattis ullamcorper. Curabitur erat lacus, pretium quis blandit a, convallis non nisl! Quisque non eros orci, id scelerisque lacus! Nam rutrum scelerisque viverra fusce.</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_tags' => array(),
  'field_image' => array(),
  'cid' => '0',
  'last_comment_name' => NULL,
  'last_comment_uid' => '1',
  'comment_count' => '0',
  'name' => 'admin',
  'picture' => '0',
  'data' => 'b:0;',
  'date' => '2012-08-18 01:18:48 -0400',
);
  return $nodes;
}
