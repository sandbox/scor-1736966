<?php
/**
 * @file
 * entity_rdf_demo.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function entity_rdf_demo_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'American',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => '1351b80d-9a2d-ce14-e12f-8fcfe1551910',
    'vocabulary_machine_name' => 'cuisines',
    'field_wikipedia_url' => array(
      'und' => array(
        0 => array(
          'url' => 'http://en.wikipedia.org/wiki/Cuisine_of_the_United_States',
          'title' => '',
          'attributes' => array(),
        ),
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Middle Eastern',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => '43687c35-9d76-6de4-991a-56ec8cc5344d',
    'vocabulary_machine_name' => 'cuisines',
    'field_wikipedia_url' => array(
      'und' => array(
        0 => array(
          'url' => 'http://en.wikipedia.org/wiki/Middle_Eastern_cuisine',
          'title' => '',
          'attributes' => array(),
        ),
      ),
    ),
  );
  $terms[] = array(
    'name' => 'paris',
    'description' => NULL,
    'format' => NULL,
    'weight' => '0',
    'uuid' => '556ae3b1-4b45-cbb4-458e-20179e125a7c',
    'vocabulary_machine_name' => 'tags',
  );
  $terms[] = array(
    'name' => 'lyon',
    'description' => NULL,
    'format' => NULL,
    'weight' => '0',
    'uuid' => 'ef62a351-810d-99d4-4d15-2fdad0f97fd2',
    'vocabulary_machine_name' => 'tags',
  );
  $terms[] = array(
    'name' => 'German',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => 'ffbc874d-aec1-2a64-a5d4-abdacb51dd56',
    'vocabulary_machine_name' => 'cuisines',
    'field_wikipedia_url' => array(
      'und' => array(
        0 => array(
          'url' => 'http://en.wikipedia.org/wiki/Cuisine_of_Germany',
          'title' => '',
          'attributes' => array(),
        ),
      ),
    ),
  );
  return $terms;
}
