<?php
/**
 * @file
 * Code for the entity_rdf_demo feature.
 */

include_once 'entity_rdf_demo.features.inc';

/**
 * Implements hook_entity_rdf_mappings().
 */
function entity_rdf_demo_entity_rdf_mappings() {
  return array(
    // node article mappings (bundle, property and field property mappings).
    'node:article' => array(
      'rdf types' => array('http://schema.org/NewsArticle'),
    ),
    'node:article:title' => array(
      'rdf properties' => array('http://schema.org/name'),
    ),
    'node:article:author' => array(
      'rdf properties' => array('http://schema.org/creator'),
      'rdf value' => '[node:author:name]',
    ),
    'node:article:created' => array(
      'rdf properties' => array('http://schema.org/created'),
      'rdf value callback' => 'date_iso8601',
      'rdf value datatype' => 'http://www.w3.org/2001/XMLSchema#dateTime',
    ),
    'node:article:body' => array(
      'field properties' => array(
        'value' => array(
          'rdf properties' => array('http://schema.org/articleBody'),
        ),
      ),
    ),

    // node event mappings (bundle, property and field property mappings).
    'node:event' => array(
      'rdf types' => array('http://schema.org/Event', 'http://schema.org/EducationEvent'),
    ),
    'node:event:title' => array(
      'rdf properties' => array('http://schema.org/name'),
    ),
    'node:event:author' => array(
      'rdf properties' => array('http://schema.org/author'),
    ),
    'node:event:body' => array(
      'field properties' => array(
        'value' => array(
          'rdf properties' => array('http://schema.org/articleBody'),
        ),
      ),
    ),
    'node:event:field_date' => array(
      'field properties' => array(
        'value' => array(
          'rdf properties' => array('http://schema.org/startDate'),
          'rdf value callback' => 'date_iso8601',
          'rdf value datatype' => 'http://www.w3.org/2001/XMLSchema#dateTime',
        ),
        'value2' => array(
          'rdf properties' => array('http://schema.org/endDate'),
          'rdf value callback' => 'date_iso8601',
          'rdf value datatype' => 'http://www.w3.org/2001/XMLSchema#dateTime',
        ),
        'duration' => array(
          'rdf properties' => array('http://schema.org/duration'),
          'rdf value callback' => 'duration_iso8601',
          'rdf value datatype' => 'http://www.w3.org/2001/XMLSchema#duration',
        ),
      ),
    ),
    // addressfield example including field properties mapping.
    'node:event:field_address' => array(
      'rdf properties' => array('http://schema.org/location'),
      'rdf types' => array('http://schema.org/PostalAddress'),
      'field properties' => array(
        'thoroughfare' => array(
          'rdf properties' => array('http://schema.org/streetAddress'),
        ),
        'locality' => array(
          'rdf properties' => array('http://schema.org/addressLocality'),
        ),
        'administrative_area' => array(
          'rdf properties' => array('http://schema.org/addressRegion'),
        ),
        'postal_code' => array(
          'rdf properties' => array('http://schema.org/postalCode'),
        ),
        'country' => array(
          'rdf properties' => array('http://schema.org/addressCountry'),
        ),
      ),
    ),
    // Link field example including field properties mapping.
    'node:event:field_link' => array(
      'field properties' => array(
        'url' => array(
          'rdf properties' => array('http://schema.org/url'),
        ),
      ),
    ),
    // entityreference field example including field properties mapping.
    'node:event:field_related_article' => array(
      'rdf properties' => array('http://schema.org/related'),
    ),
    // Image field example including field properties mapping.
    'node:event:field_image' => array(
      'rdf properties' => array('http://schema.org/image'),
      'rdf types' => array('http://schema.org/ImageObject'),
      'rdf resource' => '[node:field_image]',
      'field properties' => array(
        'alt' => array(
          'rdf properties' => array('http://schema.org/description'),
        ),
      ),
    ),

    // node restaurant mappings (bundle, property and field property mappings).
    'node:restaurant' => array(
      'rdf types' => array('http://schema.org/Restaurant', 'http://schema.org/LocalBusiness'),
    ),
    'node:restaurant:title' => array(
      'rdf properties' => array('http://schema.org/name'),
    ),
    // addressfield example including field properties mapping.
    'node:restaurant:field_address' => array(
      'rdf properties' => array('http://schema.org/location'),
      'rdf types' => array('http://schema.org/PostalAddress'),
      'field properties' => array(
        'thoroughfare' => array(
          'rdf properties' => array('http://schema.org/streetAddress'),
        ),
        'locality' => array(
          'rdf properties' => array('http://schema.org/addressLocality'),
        ),
        'administrative_area' => array(
          'rdf properties' => array('http://schema.org/addressRegion'),
        ),
        'postal_code' => array(
          'rdf properties' => array('http://schema.org/postalCode'),
        ),
        'country' => array(
          'rdf properties' => array('http://schema.org/addressCountry'),
        ),
      ),
    ),
    'node:restaurant:field_cuisine' => array(
      'rdf properties' => array('http://schema.org/servesCuisine'),
      'rdf resource' => '[node:field-cuisine:field-wikipedia-url:url]',
    ),
    'node:restaurant:field_reservations' => array(
      'rdf properties' => array('http://schema.org/acceptsReservations'),
    ),
    // Link field example featuring a field properties mapping.
    'node:restaurant:field_menu_link' => array(
      'field properties' => array(
        'url' => array(
          'rdf properties' => array('http://schema.org/menu'),
        ),
      ),
    ),
    'node:restaurant:field_telephone' => array(
      'rdf properties' => array('http://schema.org/telephone'),
    ),

    // file mappings
    'file:file' => array(
      'rdf types' => array('http://schema.org/File'),
    ),
    'file:file:name' => array(
      'rdf properties' => array('http://schema.org/filename'),
    ),

  );
}

/**
 * Formats a duration in seconds as iso8601 syntax.
 */
function duration_iso8601($data) {
  return 'PT' . $data . 'M';
}
